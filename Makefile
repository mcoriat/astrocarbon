PYTHON = python3

INPUT_DIR = inputs
ASTRO_INFRA =  $(INPUT_DIR)/astro_infra_EF.csv
ADS_AFFIDS = $(INPUT_DIR)/ads_affids.tsv

OUTPUT_DIR = outputs
L1P5_FACILITIES_EF = $(OUTPUT_DIR)/facilitiesEF.json
L1P5_AFFILIATIONS = $(OUTPUT_DIR)/astroAffiliations.json

ASTRO_ALPHAS =  $(OUTPUT_DIR)/astroAlpha_2018.csv $(OUTPUT_DIR)/astroAlpha_2019.csv $(OUTPUT_DIR)/astroAlpha_2020.csv  $(OUTPUT_DIR)/astroAlpha_2021.csv$(OUTPUT_DIR)/astroAlpha_2022.csv $(OUTPUT_DIR)/astroAlpha_2023.csv

MAKEFLAGS += --no-builtin-rules
MAKEFLAGS += --no-builtin-variables


$(L1P5_FACILITIES_EF): $(ASTRO_INFRA) scripts/EFtoJson.py
	$(PYTHON) scripts/EFtoJson.py

$(L1P5_AFFILIATIONS): $(ADS_AFFIDS) scripts/affIDtoJson.py
	$(PYTHON) scripts/affIDtoJson.py

$(OUTPUT_DIR)/astroAlpha_%.csv: scripts/adsquery.py
	$(PYTHON) scripts/adsquery.py $*

all: $(L1P5_FACILITIES_EF) $(L1P5_AFFILIATIONS) $(ASTRO_ALPHAS)

clean:
	rm $(L1P5_FACILITIES_EF) $(L1P5_AFFILIATIONS) $(ASTRO_ALPHAS)
