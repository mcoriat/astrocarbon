import json
import pandas as pd
import numpy as np
import unicodedata
import requests
from urllib.parse import urlencode


# Global parameters
token = "lTyNOsrZFprDEcgYJFKfEqiaFnVPbCoRnrxhETue"  # <== Fill-in the ADS token here
reference_years = ["2018", "2019", "2020", "2021", "2022" ,"2023"]  # Reference years
facilitiesFile = "../inputs/facilitiesDict.json" # List of facilities queried
affIdFile = "../inputs/ads_affids.tsv" # Affiliation ids list from ADS



# ======================= #
# Standardize author name #
# ======================= #
def standard_name(author):
    """
    Standardize author name
    """

    # Replace special characters in author name
    author = (
        unicodedata.normalize("NFKD", author).encode("ASCII", "ignore").decode("ascii")
    )

    # Simplify name
    names = author.split(",")
    author_std = names[0]
    if len(names) > 1:
        author_std += ", {}.".format(names[1].strip()[0:1])

    # Return
    return author_std


# ================================================ #
# Determine shared usage for a given facility      #
# ================================================ #
def get_alpha_facility(fquery, year, affId_list, database=""):
    """
    Determine usage of a given astro facility by labs worlwide
    based on ratio of number of authors in publications
    """
    # Initialise list of authors/affiliation id pairs
    pairs_all = []

    # Create data structure to hold number of authors for each affiliation ID
    authors_lab = pd.Series(data=np.zeros_like(affId_list), index=affId_list)

    # Build query string
    query = "year:{} full:{} collection:{}".format(year, fquery, database)

    # Loop until all results are returned
    start = 0
    rows = 2000

    while True:
        encoded_query = urlencode(
            {
                "q": query,
                "fq": "property:refereed",
                "start": start,
                "rows": rows,
                "fl": "year,title,author,author_count,aff_id, aff",
            }
        )

        # Query database
        result = requests.get(
            "https://api.adsabs.harvard.edu/v1/search/query?{}".format(encoded_query),
            headers={"Authorization": "Bearer " + token},
        )

        # Raise an exception if request failed (based on status code)
        result.raise_for_status()

        # format result to json
        adsdict = result.json()

        # Extract relevant attributes
        docs = adsdict["response"]["docs"]

        # Loop over publications
        for doc in docs:
            if "author_count" not in doc:
                pass
            else:
                authors = doc["author"]
                aff_ids = doc["aff_id"]
                for author, ids in zip(authors, aff_ids):
                    author = standard_name(author)
                    ids = [a.strip() for a in ids.split(";") if a.strip() != "-"]
                    for id in ids:
                        pair = id + "_" + author
                        if id in affId_list and pair not in pairs_all:
                            pairs_all.append(pair)
                            authors_lab[id] += 1

        # Exit if no more publications found
        if len(docs) < rows:
            break
        else:
            start += rows

    # Determine total number of pairs authors/aff_id and calculate alphas converted into percentage
    num_pairs_all = len(pairs_all)
    if num_pairs_all != 0:
        alphas = authors_lab / num_pairs_all * 100
    else:
        alphas = authors_lab * 100

    # Return results
    return alphas


# =================================================== #
# Get shared usage (alphas) for a given mission list  #
# =================================================== #
def get_publications(year, facilitiesFile, affIdFile, outputFile):
    """
    Get usage (in percent) of a given mission list by labs worlwide

    Validation of ADS queries is done using the 'Show Highlights' button which shows on which
    text the query triggered.
    """
    # Get full list of affiliation IDs
    affId_map = pd.read_csv(
        affIdFile,
        names=["Country", "Aff_id_parent", "Aff_id", "Inst", "Affiliation"],
        sep="\t",
        header=0,
        dtype=str,
    )
    affId_list = affId_map["Aff_id"].unique()

    # Get facilities dictionary
    with open(facilitiesFile, "r") as fp:
        facilities = json.load(fp)

    # Build dataframe to hold alpha values for all lab/facilities pair
    # Columns name are facilities name and index are affiliation ids
    data = pd.DataFrame(index=affId_list, columns=facilities.keys())

    for key in facilities:
        m = facilities[key]
        # Extract optional database
        if "database" in m:
            database = m["database"]
        else:
            database = ""

        # Get alpha values for all astro lab in the world for this facility
        alphas = get_alpha_facility(
            m["query"],
            year,
            affId_list,
            database=database,
        )
        data[key] = alphas
        print(key, " Done")

    # Remove labs with zero contribution and write to file
    indices = data.loc[(data == 0).all(axis=1)].index.tolist()
    data_filtered = data.drop(indices)
    data_filtered.to_csv(outputFile, index_label='lab_id')

    # Return
    return


# =========== #
# Entry point #
# =========== #
if __name__ == "__main__":
    import sys
    # Check that token has been set
    if token == "":
        print(
            "Please set the ADS token at the top of the script before running the script."
        )

    # ... otherwise get the publications
    else:
        year = sys.argv[1]
        outputFile = "../outputs/astroAlpha_" + year + ".csv"
        get_publications(year, facilitiesFile, affIdFile, outputFile)
        print(year, " done")
