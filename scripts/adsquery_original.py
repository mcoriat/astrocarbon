#! /usr/bin/env python
# ==========================================================================
# Query ADS database
#
# This script queries the ADS database to extract the information on the
# number of papers and the number of authors attributed to a number of
# space missions and ground-based observatories.
#
# The queries for the values quoted in the paper were done in August 2021.
#
# The script queries the ADS database with a given search string, querying
# either only the abstract and title (abs) or the full text (full). For
# the paper the full text results were used.
#
# The script also queries the official publication lists of the infrastructures
# for comparison.
#
# The query strings were setup in a way that the number of papers that use
# or mention a given infrastructure are returned. In other words, the
# numbers can be seen as infrastructure citation index. All query strings
# were validated using the interactive ADS interface by using the
# "Show highlights" button that shows on which words the query triggered.
# The strategy for defining a query string was to be as comrehensive as
# possible while pushing the number of false postives to below 1%. This
# was done by checking that no false positives exists among the first 100
# results. In some cases the paper where the returned highlights were not
# explicit enough the paper text was checked to assure that the paper indeed
# mentions the instrument.
#
# To run the script please create a Token in the ADS system and set the
# token at the top of the script. And uncomment the instruments for
# which you want to run the query in the methods get_publications_from_query_string()
# and get_publications_from_library().
#
# Copyright (C) 2021 Juergen Knoedlseder
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# ==========================================================================
import json
import subprocess
import unicodedata

# ADS token
token          = 'lTyNOsrZFprDEcgYJFKfEqiaFnVPbCoRnrxhETue'         # <== Fill-in the ADS token here
affiliation    = 'IRAP'     # <== Replace by your affiliation
affiliation_id = 'A04660'   # <== Replace by the ADS affiliation ID of your lab
reference_year = '2019'     # <== Reference year for yearly publications


# ================== #
# Get unique authors #
# ================== #
def unique_authors(authors):
    """
    Get list of unique authors
    """
    # Initialise list of unique authors
    uniques = []

    # Loop over all authors
    for author in authors:

        # Replace special characters in author name
        author = (
        unicodedata.normalize("NFKD", author).encode("ASCII", "ignore").decode("ascii")
        )

        # Simplify name
        names = author.split(',')
        name  = names[0]
        if len(names) > 1:
            name += ', %s.' % (names[1].strip()[0:1])

        # If name is not in list of unique names then append it
        if name not in uniques:
            uniques.append(name)

    # Sort unique authors (for display if requested)
    uniques.sort()

    # Return
    return uniques


# ========== #
# Send Query #
# ========== #
def query(query, type, years, irap=False, database=''):
    """
    Query ADS database
    """
    # Replace characters in query string
    query = query.replace(' ', '%20').replace('"', '%22').replace('/', '%2F').replace('-', '%2D')

    # Optionally add IRAP to query string
    if irap:
        query += '+inst:%s' % (affiliation)

    # Optionally add database
    if database != '':
        query += '+collection:%s' % database

    # Build query string
    # start= : Offset of the first result returned (default=0)
    # rows=  : How many records to return for this request (default=10, maximum=200)
    cmd = "curl -s -H 'Authorization: Bearer:%s' 'https://api.adsabs.harvard.edu/v1/search/query?q=year:%s+%s%s&fq=property:refereed&fl=year,title'" % (token, years, type, query)

    # Query database
    error, result = subprocess.getstatusoutput(cmd)

    # Extract number of entries from result
    if error == 0:

        # Build dictionary
        adsdict = json.loads(result)

        # Extract relevant arrtibutes
        status   = adsdict['responseHeader']['status']
        q        = adsdict['responseHeader']['params']['q']
        fq       = adsdict['responseHeader']['params']['fq']
        numFound = adsdict['response']['numFound']
        
        # Build result disctionary
        resultdict = {'status':   status,
                      'q':        q,
                      'fq':       fq,
                      'numFound': numFound}

    # Return resultdict
    return resultdict


# ================================================ #
# Determine number of authors for all publications #
# ================================================ #
def get_num_authors(query, type, years, irap=False, database='', debug=False):
    """
    Determine number of authors for all publications
    """
    # Initialise list of authors
    authors_all  = []
    authors_irap = []

    # Replace characters in query string
    query = query.replace(' ', '%20').replace('"', '%22').replace('/', '%2F')

    # Optionally add IRAP to query string
    if irap:
        query += '+inst:%s' % (affiliation)

    # Optionally add database
    if database != '':
        query += '+collection:%s' % database

    # Loop until all results are returned
    start = 0
    rows  = 2000
    while True:

        # Build query string
        cmd = "curl -s -H 'Authorization: Bearer:%s' 'https://api.adsabs.harvard.edu/v1/search/query?q=year:%s+%s%s&fq=property:refereed&start=%d&rows=%d&fl=year,title,author,author_count,aff_id'" % (token, years, type, query, start, rows)

        # Query database
        error, result = subprocess.getstatusoutput(cmd)

        # Stop loop if an error occured
        if error != 0:
            break

        # Build dictionary
        adsdict = json.loads(result)

        # Extract relevant arrtibutes
        status   = adsdict['responseHeader']['status']
        q        = adsdict['responseHeader']['params']['q']
        fq       = adsdict['responseHeader']['params']['fq']
        numFound = adsdict['response']['numFound']
        docs     = adsdict['response']['docs']
        numdocs  = len(docs)

        # Debug
        #print(numFound, numdocs)

        # Exit if no more publications were found
        if numdocs < 1:
            break

        # Loop over publications
        for doc in docs:
            if 'author_count' not in doc:
                pass
            else:
                author_count = doc['author_count']
                authors      = doc['author']
                aff_ids      = doc['aff_id']
                for i, author in enumerate(authors):
                    if author not in authors_all:
                        authors_all.append(author)
                    if affiliation_id in aff_ids[i]:
                        if author not in authors_irap:
                            authors_irap.append(author)

        # Increment start
        start += rows

    # Determine number of authors
    authors_all      = unique_authors(authors_all)
    authors_irap     = unique_authors(authors_irap)
    num_authors_all  = len(authors_all)
    num_authors_irap = len(authors_irap)

    # Debug option: print IRAP authors
    if debug:
        print(authors_irap)
        #print(authors_all)

    # Build result dictionary
    result = {'numFound': numFound, 'numAuthorsAll': num_authors_all, 'numAuthorsIRAP': num_authors_irap}

    # Return results
    return result


# ======================================== #
# Get ADS publications for a given mission #
# ======================================== #
def get_publications(mission, authors=False, debug=False):
    """
    Get ADS publications for a given mission

    Validation of ADS queries is done using the 'Show Highlights' button which shows on which
    text the query triggered.
    """
    # Mission dictionary
    missions = [# PSP: ADS Query validated
                {'name':     'PSP',
                 'start':    '2018',
                 'query':    '(="Parker Solar Probe" OR "NASA PSP")',
                 'database': '(astronomy%20or%20physics)'
                },
                # SDO: ADS Query validated
                {'name':     'SDO',
                 'start':    '2010',
                 'query':    '(="Solar Dynamics Observatory" OR "HMI/SDO" OR "AIA/SDO" OR "EVE/SDO")',
                 'database': '(astronomy%20or%20physics)'
                },
                # SoHo: ADS Query validated
                {'name':     'SOHO',
                 'start':    '1995',
                 'query':    '(="SOHO")',
                 'database': '(astronomy%20or%20physics)'
                },

                # MMD: ADS Query validated
                {'name':     'MMS',
                 'start':    '2015',
                 'query':    '(="Magnetospheric Multiscale" OR "NASA/MMS")',
                 'database': '(astronomy%20or%20physics)'
                },
                # Cluster: ADS Query validated
                {'name':     'Cluster',
                 'start':    '2000',
                 'query':    '(="Cluster spacecraft" OR ="Cluster mission" OR ="Cluster satellite constellation")',
                 'database': '(astronomy%20or%20physics)'
                },
                # DoubleStar: ADS Query validated
                {'name':     'DoubleStar',
                 'start':    '2004',
                 'query':    '(="Double Star mission" OR ="Double Star spacecrafts" OR ="Double Star satellites" OR ="Double Star TC" OR "TC-1 satellite" OR "TC-2 satellite")',
                 'database': '(astronomy%20or%20physics)'
                },
                # DEMETER: ADS Query validated
                {'name':     'DEMETER',
                 'start':    '2004',
                 'query':    '(="DEMETER spacecraft" OR ="DEMETER mission" OR ="DEMETER satellite")',
                 'database': '(astronomy%20or%20physics)'
                },
                # STEREO: ADS Query validated
                {'name':     'STEREO',
                 'start':    '2006',
                 'query':    '(="STEREO")',
                 'database': '(astronomy%20or%20physics)'},
                # ACE: ADS Query validated
                {'name':     'ACE',
                 'start':    '1997',
                 'query':    '(="Advanced Composition Explorer" OR ="ACE mission" OR ="ACE spacecraft" OR ="ACE satellite")',
                 'database': '(astronomy%20or%20physics)'},
                # IMP-8: ADS Query validated
                {'name':     'IMP-8',
                 'start':    '1973',
                 'query':    '(="Interplanetary Monitoring Platform 8" OR ="IMP-8")',
                 'database': '(astronomy%20or%20physics)'
                },
                # GEOTAIL: ADS Query validated
                {'name':     'GEOTAIL',
                 'start':    '1992',
                 'query':    '(="Geotail")',
                 'database': '(astronomy%20or%20physics)'
                },
                # WIND: ADS Query validated
                {'name':     'WIND',
                 'start':    '1994',
                 'query':    '(="Wind mission" OR ="Wind spacecraft" OR ="Wind satellite" OR ="WIND/WAVES" OR ="Wind/3DP" OR ="Wind/MFI" OR ="Wind/SWE" OR ="Wind/KONUS" OR ="Wind/TGRS" OR ="Wind/EPACT" OR ="Wind/SMS")',
                 'database': '(astronomy%20or%20physics)'
                },
                # TIMED: ADS Query validated
                {'name':     'TIMED',
                 'start':    '2002',
                 'query':    '(="TIMED")',
                 'database': '(astronomy%20or%20physics)'
                },

                # VEX: ADS Query validated
                {'name':     'VEX',
                 'start':    '2005',
                 'query':    '(="Venus Express" OR ="VEX mission" OR ="VEX spacecraft" OR ="VEX probe")',
                 'database': '(astronomy%20or%20physics)'
                },
                # MAVEN: ADS Query validated
                {'name':     'MAVEN',
                 'start':    '2014',
                 'query':    '(="Mars Atmosphere and Volatile EvolutioN" OR ="MAVEN")',
                 'database': '(astronomy%20or%20physics)'
                },
                # MEX: ADS Query validated
                {'name':     'MEX',
                 'start':    '2003',
                 'query':    '(="Mars Express" OR ="MEX mission" OR ="MEX spacecraft" OR ="MEX probe")',
                 'database': '(astronomy%20or%20physics)'
                },
                # InSight: ADS Query validated
                {'name':     'InSight',
                 'start':    '2019',
                 'query':    '(="Interior Exploration using Seismic Investigations, Geodesy and Heat Transport" OR ="InSight mission" OR ="NASA/InSight" OR ="InSight Mars" OR ="InSight/SEIS" OR ="InSight/HP3" OR ="InSight/RISE" OR ="InSight/TWINS" OR ="InSight/LaRRI" OR ="InSight/IDA" OR ="InSight/IDC" OR ="InSight/ICC")',
                 'database': '(astronomy%20or%20physics)'
                },
                # MSL: ADS Query validated
                {'name':     'MSL',
                 'start':    '2013',
                 'query':    '(="Mars Science Laboratory" OR ="Curiosity rover" OR ="MSL mission")',
                 'database': '(astronomy%20or%20physics)'
                },
                # MRO: ADS Query validated
                {'name':     'MRO',
                 'start':    '2006',
                 'query':    '(="Mars Reconnaissance Orbiter" OR "MRO Mars" OR "MRO Orbiter" OR "MRO spacecraft")',
                 'database': '(astronomy%20or%20physics)'
                },
                # Galileo: ADS Query validated
                {'name':     'Galileo',
                 'start':    '1990',
                 'query':    '(="Galileo mission" OR ="Galileo spacecraft" OR ="Galileo orbiter" OR ="NASA/Galileo"  OR ="Galileo entry probe" -GNSS)',
                 'database': '(astronomy%20or%20physics)'
                },
                # Juno: ADS Query validated
                {'name':     'Juno',
                 'start':    '2012',
                 'query':    '(="Juno mission"  OR ="Juno spacecraft" OR ="Juno orbiter" OR ="NASA/Juno" OR ="Juno MWR" OR ="Juno JIRAM" OR ="Juno MAG" OR ="Juno GS" OR ="Juno JADE" OR ="Juno JEDI" OR ="Juno Waves" OR ="Juno UVS" OR ="Juno JCM")',
                 'database': '(astronomy%20or%20physics)'
                },
                # Cassini: ADS Query validated
                {'name':     'Cassini',
                 'start':    '1998',
                 'query':    '(="Cassini mission" OR ="Cassini spacecraft" OR ="Cassini orbiter" OR ="Cassini-Huygens" OR ="Huygens module" OR ="Huygens probe" OR ="Huygens lander")',
                 'database': '(astronomy%20or%20physics)'
                },
                # Rosetta: ADS Query validated
                {'name':     'Rosetta',
                 'start':    '2004',
                 'query':    '(="Rosetta mission" OR ="Rosetta spacecraft" OR "Rosetta Churyumov-Gerasimenko" OR "Philae Churyumov-Gerasimenko")',
                 'database': '(astronomy%20or%20physics)'
                },
                # Dawn: ADS Query validated
                {'name':     'Dawn',
                 'start':    '2008',
                 'query':    '(="Dawn mission" OR ="Dawn spacecraft" OR ="Dawn orbiter" OR ="NASA/Dawn" OR ="Dawn Vesta" OR ="Dawn Ceres")',
                 'database': '(astronomy%20or%20physics)'
                },

                # TESS: ADS Query validated
                {'name':     'TESS',
                 'start':    '2018',
                 'query':    '(="TESS" NOT "transition edge sensors")',
                 'database': 'astronomy'},
                # GAIA: ADS Query validated
                {'name':     'GAIA',
                 'start':    '2013',
                 'query':    '(="GAIA")',
                 'database': 'astronomy'},
                # Hipparcos: ADS Query validated
                {'name':     'Hipparcos',
                 'start':    '1989',
                 'query':    '(="HIgh Precision PARallax COllecting Satellite" OR ="Hipparcos mission" OR ="Hipparcos spacecraft" OR ="Hipparcos satellite" OR ="Hipparcos parallax" OR ="ESA/Hipparcos")',
                 'database': 'astronomy'},
                # Spitzer: ADS Query validated
                {'name':     'Spitzer',
                 'start':    '2003',
                 'query':    '(="Spitzer Space Telescope" OR ="Spitzer mission" OR ="Spitzer observatory" OR ="Spitzer telescope" OR ="Spitzer satellite" OR ="Spitzer spacecraft")',
                 'database': 'astronomy'},
                # GALEX: ADS Query validated
                {'name':      'GALEX',
                 'start':     '2003',
                  'query':    '(="GALEX")',
                  'database': 'astronomy'},
                # Kepler: ADS Query validated
                {'name':     'Kepler',
                 'start':    '2009',
                 'query':    '(="Kepler space telescope" OR ="Kepler mission" OR ="Kepler observatory" OR ="Kepler spacecraft" OR ="Kepler satellite" OR ="Kepler telescope")',
                 'database': 'astronomy'},

                # Akari: ADS Query validated
                {'name':     'Akari',
                 'start':    '2006',
                 'query':    '(="Akari" OR ="ASTRO-F")',
                 'database': 'astronomy'},
                # Herschel: ADS Query validated
                {'name':     'Herschel',
                 'start':    '2009',
                 'query':    '(="Herschel Space Observatory" OR ="Herschel mission" OR ="Herschel observatory" OR ="Herschel spacecraft" OR ="Herschel satellite" OR ="ESA/Herschel" OR ="Herschel/PACS" OR ="Herschel/SPIRE" OR ="Herschel/HIFI" NOT ="William Herschel Telescope")',
                 'database': 'astronomy'},
                # WISE: ADS Query validated
                {'name':     'WISE',
                 'start':    '2009',
                 'query':    '(="Wide-field Infrared Survey Explorer" OR ="WISE")',
                 'database': 'astronomy'},

                # HST: ADS Query validated
                {'name':     'HST',
                 'start':    '1990',
                 'query':    '(="HST")',
                 'database': 'astronomy'},
                # Planck: ADS Query validated
                {'name':     'Planck',
                 'start':    '2009',
                 'query':    '(="Planck observatory" OR ="Planck satellite" OR ="Planck mission" OR ="Planck telescope" OR ="ESA/Planck" OR ="Planck all-sky survey" OR ="Planck CMB" OR ="Planck/HFI" OR ="Planck/LFI")',
                 'database': 'astronomy'},

                # ROSAT: ADS Query validated
                {'name':     'ROSAT',
                 'start':    '1990',
                 'query':    '(="ROSAT")',
                 'database': 'astronomy'},
                # RXTE: ADS Query validated
                {'name':     'RXTE',
                 'start':    '1996',
                 'query':    '(="RXTE" OR ="Rossi X-ray Timing Explorer")',
                 'database': 'astronomy'},
                # XMM: ADS Query validated
                {'name':     'XMM',
                 'start':    '1999',
                 'query':    '(="XMM" OR ="X-ray Multi-Mirror Mission")',
                 'database': 'astronomy'},
                # Chandra: ADS Query validated
                {'name':     'Chandra',
                 'start':    '1999',
                 'query':    '(="Chandra X-ray Observatory" OR ="CXO" OR ="Chandra mission" OR ="Chandra satellite" OR ="Chandra telescope" OR ="Chandra spacecraft" OR ="Chandra observatory" OR "Chandra X-ray" OR ="Chandra data" OR ="Chandra image" OR ="Chandra spectrum" OR ="NASA/Chandra" OR ="Chandra/ACIS" OR ="Chandra/HRC" OR ="Chandra/LETG" OR ="Chandra/HETG")',
                 'database': 'astronomy'},
                # NICER: ADS Query validated
                {'name':     'NICER',
                 'start':    '2017',
                 'query':    '(="NICER" OR ="Neutron star Interior Composition Explorer")',
                 'database': 'astronomy'},
                # NuSTAR: ADS Query validated
                {'name':     'NUSTAR',
                 'start':    '2012',
                 'query':    '(="NuSTAR" NOT ="FAIR" NOT ="GSI")',
                 'database': 'astronomy'},
                # AstroSat: ADS Query validated
                {'name':     'AstroSat',
                 'start':    '2015',
                 'query':    '(="AstroSat")',
                 'database': 'astronomy'},
                # SWIFT: ADS Query validated
                {'name':     'SWIFT',
                 'start':    '2004',
                 'query':    '(="Swift mission" OR ="Swift observatory" OR ="Swift satellite" OR ="Swift spacecraft" OR ="Swift data" OR ="NASA/Swift" OR ="Swift/BAT" OR ="Swift/XRT" OR ="Swift/UVOT")',
                 'database': 'astronomy'},
                # Suzaku: ADS Query validated
                {'name':     'Suzaku',
                 'start':    '2005',
                 'query':    '(="Suzaku")',
                 'database': 'astronomy'},
                # INTEGRAL: ADS Query validated
                {'name':     'INTEGRAL',
                 'start':    '2002',
                 'query':    '(="INTErnational Gamma-Ray Astrophysics Laboratory" OR ="INTEGRAL mission" OR ="INTEGRAL observatory" OR ="INTEGRAL satellite" OR ="INTEGRAL spacecraft" OR ="INTEGRAL data" OR ="INTEGRAL image" OR ="INTEGRAL spectrum" OR ="INTEGRAL discover" OR ="ESA/INTEGRAL" OR ="INTEGRAL/IBIS" OR ="INTEGRAL/SPI" OR ="INTEGRAL/JEMX" OR ="INTEGRAL/OMC")',
                 'database': 'astronomy'},
                # Fermi: ADS Query validated
                {'name':     'Fermi',
                 'start':    '2008',
                 'query':    '(="Fermi Gamma-ray Space Telescope" OR ="Fermi mission" OR ="Fermi observatory" OR ="Fermi satellite" OR ="Fermi spacecraft" OR ="Fermi data" OR ="Fermi image" OR ="Fermi spectrum" OR ="NASA/Fermi" OR ="Fermi/LAT" OR ="Fermi/GBM")',
                 'database': 'astronomy'},

                #
                # Ground-based observatories
                # ==========================
                # Themis: ADS Query validated
                {'name':     'THEMIS',
                 'start':    '1996',
                 'query':    '(="THEMIS observatory" OR ="THEMIS telescope" OR ="THEMIS Teide" OR ="THEMIS Tenerife" OR ="French-Italian THEMIS" OR ="French THEMIS")',
                 'database': 'astronomy'},
                # MeteoSpace: ADS Query validated
                {'name':     'MeteoSpace',
                 'start':    '2000',
                 'query':    '(="MeteoSpace")',
                 'database': 'astronomy'},
                # MLSO: ADS Query validated
                {'name':     'MLSO',
                 'start':    '1965',
                 'query':    '(="MLSO" OR ="Mauna Loa Solar Observatory")',
                 'database': 'astronomy'},
                # Meudon: ADS Query validated
                {'name':     'Meudon',
                 'start':    '1892',
                 'query':    '(="Meudon spectroheliograph" OR ="spectroheliograph Paris")',
                 'database': 'astronomy'},

                # SuperDarn: ADS Query validated
                {'name':     'SuperDARN',
                 'start':    '1992',
                 'query':    '(="SuperDARN" OR ="Super Dual Auroral Radar Network")',
                 'database': '(astronomy%20or%20physics)'},

                # HCT: ADS Query validated
                {'name':     'HCT',
                 'start':    '2001',
                 'query':    '(="Himalayan Chandra Telescope")',
                 'database': 'astronomy'},
                # Tillighast: ADS Query validated
                {'name':     'Tillinghast',
                 'start':    '1969',
                 'query':    '(="Tillinghast")',
                 'database': 'astronomy'},
                # SPM: ADS Query validated
                {'name':     'SPM',
                 'start':    '1970',
                 'query':    '((="OAN-SPM" OR ="SPM" OR ="San Pedro Martir") AND (="1.5m telescope" OR ="1.5-m telescope" OR ="Harold Johnson Telescope" OR ="Harold L. Johnson Telescope"))',
                 'database': 'astronomy'},
                # Lijiang: ADS Query validated
                {'name':     'Lijiang',
                 'start':    '2008',
                 'query':    '((="Lijiang") AND (="2.4m telescope" OR ="2.4-m telescope"))',
                 'database': 'astronomy'},
                # Pic-du-Midi: ADS Query validated
                {'name':     'PdM',
                 'start':    '1963',
                 'query':    '(="1 m telescope Pic du Midi")',
                 'database': 'astronomy'},
                # BOAO: ADS Query validated
                {'name':     'BOAO',
                 'start':    '1996',
                 'query':    '(="BOAO" NOT "SOFT" OR ="Bohyunsan Optical Astronomy Observatory")',
                 'database': 'astronomy'},
                # Xinglong: ADS Query validated
                {'name':     'Xinglong',
                 'start':    '1974',
                 'query':    '((="Xinglong Station" OR ="Xonlong Station") AND (="2.16m" OR ="2.16-m" OR ="BFOSC" OR ="OMR" OR ="HRS"))',
                 'database': 'astronomy'},
                # Nanshan: ADS Query validated
                {'name':     'Nanshan',
                 'start':    '2013',
                 'query':    '(="NOWT" OR ="Nanshan One-meter Wide field Telescope" OR ="Nanshan 1m" OR ="Nanshan 1 m")',
                 'database': 'astronomy'},
                # TBL: ADS Query validated
                {'name':     'TBL',
                 'start':    '1980',
                 'query':    '(="Telescope Bernard Lyot" OR ="Bernard Lyot Telescope" OR ="TBL Pic du Midi")',
                 'database': 'astronomy'},
                # C2PU: ADS Query validated
                {'name':     'C2PU',
                 'start':    '2014',
                 'query':    '(="C2PU" OR ="Centre Pedagogique Planete et Unviers")',
                 'database': 'astronomy'},
                # OGLE: ADS Query validated
                {'name':     'OGLE',
                 'start':    '1997',
                 'query':    '(="OGLE" OR ="Optical Gravitational Lensing Experiment" OR ="Warsaw telescope")',
                 'database': 'astronomy'},
                # KMTNet: ADS Query validated
                {'name':     'KMTNet',
                 'start':    '2015',
                 'query':    '(="KMTNet" OR ="Korea Microlensing Telescope Network")',
                 'database': 'astronomy'},
                # OHP: ADS Query validated
                {'name':     'OHP',
                 'start':    '1958',
                 'query':    '(="OHP 1.93m" OR ="OHP 1.93-m" OR ="OHP 193cm" OR ="OHP 193-cm" OR ="OHP/SOPHIE" OR ="OHP/MISTRAL" OR ="OHP/ELODIE" OR ="1.93m OHP" OR ="1.93-m OHP" OR ="193cm OHP" OR ="193-cm OHP" OR ="SOPHIE/OHP" OR ="MISTRAL/OHP" OR ="ELODIE/OHP" OR ="Observatoire de Haute Provence 1.93m" OR ="Observatoire de Haute Provence 1.93-m" OR ="Observatoire de Haute Provence 193cm" OR ="Observatoire de Haute Provence 193-cm" OR ="Observatoire de Haute Provence SOPHIE" OR ="Observatoire de Haute Provence MISTRAL" OR ="Observatoire de Haute Provence ELODIE" OR ="1.93m Observatoire de Haute Provence" OR ="1.93-m Observatoire de Haute Provence" OR ="193cm Observatoire de Haute Provence" OR ="193-cm Observatoire de Haute Provence" OR ="SOPHIE Observatoire de Haute Provence" OR ="MISTRAL Observatoire de Haute Provence" OR ="ELODIE Observatoire de Haute Provence")',
                 'database': 'astronomy'},
                # AAT: ADS Query validated
                {'name':     'AAT',
                 'start':    '1974',
                 'query':    '(="AAT telescope" OR ="AAT 3.9m" OR ="AAT 3.9 m" OR ="3.9m AAT" OR ="3.9 m AAT" OR ="Anglo-Australian Telescope")',
                 'database': 'astronomy'},
                # CFHT: ADS Query validated
                {'name':     'CFHT',
                 'start':    '1979',
                 'query':    '(="CFHT")',
                 'database': 'astronomy'},
                # La Silla: ADS Query validated
                {'name':     'LaSilla',
                 'start':    '1977',
                 'query':    '(="ESO 3.6m" NOT "NTT" OR ="ESO 3.6 m" NOT "NTT" OR ="La Silla 3.6m" NOT "NTT" OR ="La Silla 3.6 m" NOT "NTT" OR ="3.6m ESO" NOT "NTT" OR ="3.6 m ESO" NOT "NTT" OR ="3.6m La Silla" NOT "NTT" OR ="3.6 m La Silla" NOT "NTT" OR ="HARPS" NOT "HARPS-N" OR ="TIMMI-2")',
                 'database': 'astronomy'},

                # NRO: ADS Query validated
                {'name':     'NRO',
                 'start':    '1982',
                 'query':    '((="NRO" OR ="Nobeyama Radio Observatory") AND (="45m" OR ="45-m" OR ="single-dish" OR ="radio" NOT "interferometric"))',
                 'database': 'astronomy'},
                # SOFIA: ADS Query validated
                {'name':     'SOFIA',
                 'start':    '2011',
                 'query':    '(="SOFIA" NOT "Science Center" OR ="Stratospheric Observatory for Infrared Astronomy")',
                 'database': 'astronomy'},
                # IRAM: ADS Query validated
                {'name':     'IRAM',
                 'start':    '1990',
                 'query':    '(="IRAM" OR ="NOEMA" OR ="NOrthern Extended Millimeter Array")',
                 'database': 'astronomy'},
                # Noto: ADS Query validated
                {'name':     'Noto',
                 'start':    '1988',
                 'query':    '("Noto radio" OR "Noto telescope" OR "Noto VLBI" OR "Noto GHz" OR "Noto 32m" OR "Noto 32-m")',
                 'database': 'astronomy'},
                # TRAO: ADS Query validated
                {'name':     'TRAO',
                 'start':    '1986',
                 'query':    '(="TRAO" OR ="Taeduk Radio Astronomy Observatory")',
                 'database': 'astronomy'},
                # SMA: ADS Query validated
                {'name':     'SMA',
                 'start':    '2003',
                 'query':    '(="Submillimeter Array (SMA)" OR ="SMA data" OR ="SMA images")',
                 'database': 'astronomy'},
                # LOFAR: ADS Query validated
                {'name':     'LOFAR',
                 'start':    '2012',
                 'query':    '(="LOFAR")',
                 'database': 'astronomy'},
                # APEX: ADS Query validated
                {'name':     'APEX',
                 'start':    '2005',
                 'query':    '(="APEX")',
                 'database': 'astronomy'},

                # ALMA: ADS Query validated
                {'name':     'ALMA',
                 'start':    '2011',
                 'query':    '("=ALMA" NOT ("NAOJ ALMA Scientific Research Grant" OR "ALMA Regional Center"))',
                 'database': 'astronomy'},
                # ACT: ADS Query validated
                {'name':     'ACT',
                 'start':    '2007',
                 'query':    '("=ACT -IACT" NOT "Cherenkov" NOT "ACT-America")',
                 'database': 'astronomy'},
                # JCMT: ADS Query validated
                {'name':     'JCMT',
                 'start':    '1987',
                 'query':    '(="JCMT")',
                 'database': 'astronomy'},
                # VLT: ADS Query validated
                {'name':     'VLT',
                 'start':    '1999',
                 'query':    '(="VLT" NOT "very low titanium")',
                 'database': 'astronomy'},
                # GTC: ADS Query validated
                {'name':     'GTC',
                 'start':    '2009',
                 'query':    '(="GTC" NOT "PIC code" NOT "DNA")',
                 'database': 'astronomy'},
                # Gemini-South: ADS Query validated
                {'name':     'Gemini',
                 'start':    '2000',
                 'query':    '(="Gemini South" OR ="Gemini-S")',
                 'database': 'astronomy'},
                # TAROT: ADS Query validated
                {'name':     'TAROT',
                 'start':    '1998',
                 'query':    '(="TAROT" OR ="Telescope a action rapid pour les objets transitoires")',
                 'database': 'astronomy'},
                # ATCA: ADS Query validated
                {'name':     'ATCA',
                 'start':    '1988',
                 'query':    '(="ATCA")',
                 'database': 'astronomy'},
                # MeerKAT: ADS Query validated
                {'name':     'MeerKAT',
                 'start':    '2018',
                 'query':    '(="MeerKAT")',
                 'database': 'astronomy'},
                # VLA: ADS Query validated
                {'name':     'VLA',
                 'start':    '1980',
                 'query':    '(="VLA")',
                 'database': 'astronomy'},
                # VLBA: ADS Query validated
                {'name':     'VLBA',
                 'start':    '1993',
                 'query':    '(="VLBA")',
                 'database': 'astronomy'},
                # GBT: ADS Query validated
                {'name':     'GBT',
                 'start':    '2001',
                 'query':    '(="GBT" NOT "=Gauss-Bonnet theorem")',
                 'database': 'astronomy'},
                # EHT: ADS Query validated
                {'name':     'EHT',
                 'start':    '2014',
                 'query':    '(="EHT" NOT "=Gauge fields")',
                 'database': 'astronomy'},
                # LMT: ADS Query validated (some uncertainty due to "LMT" query)
                {'name':     'LMT',
                 'start':    '2014',
                 'query':    '((="LMT" NOT "LMT-K" NOT "local mean time" AND ("telescope" OR "50m" OR "50-m")) OR "=Large Millimeter Telescope")',
                 'database': 'astronomy'},

                # HESS: ADS Query validated
                {'name':     'HESS',
                 'start':    '2003',
                 'query':    '("=HESS -Hess" NOT "Hessian" OR ="H.E.S.S.")',
                 'database': 'astronomy'}
               ]

    # Search for mission in dictionary
    for m in missions:
        if m['name'] == mission:

            # Extract optional database
            if 'database' in m:
                database = m['database']
            else:
                database = ''

            # Make queries
            full_irap     = query(m['query'], 'full:', '%s-%s' % (m['start'], reference_year), irap=True, database=database)
            full_all      = query(m['query'], 'full:', '%s-%s' % (m['start'], reference_year), irap=False, database=database)
            abs_irap      = query(m['query'], 'abs:',  '%s-%s' % (m['start'], reference_year), irap=True, database=database)
            abs_all       = query(m['query'], 'abs:',  '%s-%s' % (m['start'], reference_year), irap=False, database=database)
            full2019_irap = query(m['query'], 'full:', reference_year, irap=True, database=database)
            full2019_all  = query(m['query'], 'full:', reference_year, irap=False, database=database)
            abs2019_irap  = query(m['query'], 'abs:',  reference_year, irap=True, database=database)
            abs2019_all   = query(m['query'], 'abs:',  reference_year, irap=False, database=database)

            # Get number of authors and print results
            if authors:

                # Get number of authors
                full_authors     = get_num_authors(m['query'], 'full:', '%s-%s' % (m['start'], reference_year), irap=False, database=database, debug=debug)
                full2019_authors = get_num_authors(m['query'], 'full:', reference_year, irap=False, database=database, debug=debug)
                alpha = full2019_authors['numAuthorsIRAP'] / full2019_authors['numAuthorsAll']
                # Print result
                print('%-10s %5s  %5d %5d   %5d %5d    %5d %5d    %5d %5d        %5d %5d         %5d %5d' %
                      (m['name'], m['start'],
                       abs2019_irap['numFound'],
                       abs2019_all['numFound'],
                       full2019_irap['numFound'],
                       full2019_all['numFound'],
                       full2019_authors['numAuthorsIRAP'],
                       full2019_authors['numAuthorsAll'],
                       abs_irap['numFound'],
                       abs_all['numFound'],
                       full_irap['numFound'],
                       full_all['numFound'],
                       full_authors['numAuthorsIRAP'],
                       full_authors['numAuthorsAll'],))
                
                print('%-10s %5f' % (m['name'], alpha))

            # ... otherwise just print results
            else:
                print('%-10s %5s  %5d %5d   %5d %5d    %5d %5d        %5d %5d' %
                      (m['name'], m['start'],
                       abs2019_irap['numFound'],
                       abs2019_all['numFound'],
                       full2019_irap['numFound'],
                       full2019_all['numFound'],
                       abs_irap['numFound'],
                       abs_all['numFound'],
                       full_irap['numFound'],
                       full_all['numFound']))

    # Return
    return


# ======================================== #
# Get ADS publications for a given mission #
# ======================================== #
def get_library(mission):
    """
    Get ADS publications for a given mission from library
    """
    # Mission dictionary
    missions = [{'name': 'SOHO',       'query': 'docs(library/HLx1YisxRhyufHOCBhs_Gg)',   'start': '1995'},
                {'name': 'Cluster',    'query': 'docs(library/m7Fa10y8SjGlGuVtTJrI-Q)',   'start': '2000'},
                {'name': 'DoubleStar', 'query': 'docs(library/vY6JVTqySq2b_6ZQPMVYEQ)',   'start': '2004'},
                {'name': 'VEX',        'query': 'docs(library/OQ2qYAJfSAaGOC6n_P-rBQ)',   'start': '2005'},
                {'name': 'MEX',        'query': 'docs(library/OYxwDfCrTuOnzsQ01bcy3g)',   'start': '2003'},
                {'name': 'Cassini',    'query': 'docs(library/GMIKhqj3Sb-paudruY9dHg)',   'start': '1998'},
                {'name': 'Rosetta',    'query': 'docs(library/U_xTEPzmTwCaDqXsYzgmqA)',   'start': '2004'},
                {'name': 'GAIA',       'query': 'docs(library/fWFE_JYLRZG2jwgwKetH8w)',   'start': '2013'},
                {'name': 'Hipparcos',  'query': 'docs(library/6pdYaNx2QO6l39auztu15A)',   'start': '1989'},
                {'name': 'AKARI',      'query': 'docs(library/Gsa2LHf2Srigb7E5OgQ8AA)',   'start': '2006'},
                {'name': 'Herschel',   'query': 'docs(library/Ozqb7LmUR-mm1rFKIRlg0A)',   'start': '2009'},
                {'name': 'HST',        'query': 'bibgroup:HST',                           'start': '1990'},
                {'name': 'Planck',     'query': 'docs(library/iOb_mNpHStaeLCxL_M9Cyw)',   'start': '2009'},
                {'name': 'XMM',        'query': 'docs(library/H1FbvBQoQ2ioItwFId3O7w)',   'start': '1999'},
                {'name': 'Chandra',    'query': 'bibgroup:Chandra',                       'start': '1999'},
                {'name': 'NICER',      'query': 'docs(library/9qNzvbWNRJC3E-6c0p64Rw)',   'start': '2017'},
                {'name': 'Suzaku',     'query': 'docs(library/7pxjWHCxQGiAWsA_6XFKIw .)', 'start': '2005'},
                {'name': 'INTEGRAL',   'query': 'docs(library/5oYLIENpRxC8S38Btgo-fw)',   'start': '2002'},

                {'name': 'LaSilla',  'start': '1977', 'query': 'bibgroup:"ESO/Telescopes" AND (full:("HARPS") OR full:("HARPSpol"))'},
                {'name': 'SMA',      'start': '2003', 'query': 'bibgroup:SMA'},
                {'name': 'LOFAR',    'start': '2012', 'query': 'full:("designed and constructed by ASTRON") OR title:"LOFAR" year:2004-2020 property:refereed -bibstem:("AN" OR "MNRAS.tmp")'},

                {'name': 'ALMA',     'start': '2011', 'query': 'bibgroup:ALMA'},
                {'name': 'CFHT',     'start': '1979', 'query': 'bibgroup:CFHT'},
                {'name': 'JCMT',     'start': '1987', 'query': 'docs(library/FFUnBRxWROK-NauemkAjlQ)'},
                {'name': 'VLT',      'start': '1999', 'query': 'bibgroup:"ESO/Telescopes" AND full:("VLT")'},

                {'name': 'IRAM2019', 'start': '1990', 'query': 'docs(library/aoxETn1HSYe9WbeGmDkK5Q)'}

               ]

    # Search for mission in dictionary
    for m in missions:
        if m['name'] == mission:

            # Make queries
            irap     = query(m['query'], '', '%s-2019' % m['start'], irap=True)
            all      = query(m['query'], '', '%s-2019' % m['start'], irap=False)
            irap2019 = query(m['query'], '', '2019', irap=True)
            all2019  = query(m['query'], '', '2019', irap=False)

            # Print result
            print('%-10s %5s  %5d %5d   %5d %5d' %
                  (m['name'], m['start'],
                   irap2019['numFound'],
                   all2019['numFound'],
                   irap['numFound'],
                   all['numFound']))

    # Return
    return


# ==================================== #
# Get ADS publications by query string #
# ==================================== #
def get_publications_by_query_string(authors=False):
    """
    """
    # Print header
    if authors:
        print('Mission    Start   abs (%s)    full (%s)    auth (%s)    abs (start-%s)   full (start-%s)   auth (start-%s)' % (reference_year,reference_year,reference_year,reference_year,reference_year,reference_year))
        print('                   IRAP   all    IRAP   all     IRAP   all     IRAP   all         IRAP   all          IRAP   all')
    else:
        print('Mission    Start   abs (%s)    full (%s)    abs (start-%s)   full (start-%s)' % (reference_year,reference_year,reference_year,reference_year))
        print('                   IRAP   all    IRAP   all     IRAP   all         IRAP   all')

    # Query ADS for space missions
    get_publications('PSP', authors=authors)
    get_publications('SDO', authors=authors)
    get_publications('SOHO', authors=authors)

    get_publications('MMS', authors=authors)
    get_publications('Cluster', authors=authors)
    get_publications('DoubleStar', authors=authors)
    get_publications('DEMETER', authors=authors)
    get_publications('STEREO', authors=authors)
    get_publications('ACE', authors=authors)
    get_publications('IMP-8', authors=authors)
    get_publications('GEOTAIL', authors=authors)
    get_publications('WIND', authors=authors)
    get_publications('TIMED', authors=authors)

    get_publications('VEX', authors=authors)
    get_publications('MAVEN', authors=authors)
    get_publications('MEX', authors=authors)
    get_publications('InSight', authors=authors)
    get_publications('MSL', authors=authors)
    get_publications('MRO', authors=authors)
    get_publications('Galileo', authors=authors)
    get_publications('Juno', authors=authors)
    get_publications('Cassini', authors=authors)
    get_publications('Rosetta', authors=authors)
    get_publications('Dawn', authors=authors)

    get_publications('TESS', authors=authors)
    get_publications('GAIA', authors=authors)
    get_publications('Hipparcos', authors=authors)
    get_publications('Spitzer', authors=authors)
    get_publications('GALEX', authors=authors)
    get_publications('Kepler', authors=authors)

    get_publications('Akari', authors=authors)
    get_publications('Herschel', authors=authors)
    get_publications('WISE', authors=authors)

    get_publications('HST', authors=authors)
    get_publications('Planck', authors=authors)

    get_publications('ROSAT', authors=authors)
    get_publications('RXTE', authors=authors)
    get_publications('XMM', authors=authors)
    get_publications('Chandra', authors=authors)
    get_publications('NICER', authors=authors)
    get_publications('NUSTAR', authors=authors)
    get_publications('AstroSat', authors=authors)
    get_publications('SWIFT', authors=authors)
    get_publications('Suzaku', authors=authors)
    get_publications('INTEGRAL', authors=authors)
    get_publications('Fermi', authors=authors)

    # Query ADS for ground-based observatories
    get_publications('THEMIS', authors=authors)
    get_publications('MeteoSpace', authors=authors)
    get_publications('MLSO', authors=authors)
    get_publications('Meudon', authors=authors)

    get_publications('SuperDARN', authors=authors)

    get_publications('HCT', authors=authors)
    get_publications('Tillinghast', authors=authors)
    get_publications('SPM', authors=authors)
    get_publications('Lijiang', authors=authors)
    get_publications('PdM', authors=authors)
    get_publications('BOAO', authors=authors)
    get_publications('Xinglong', authors=authors)
    get_publications('Nanshan', authors=authors)
    get_publications('TBL', authors=authors)
    get_publications('C2PU', authors=authors)
    get_publications('OGLE', authors=authors)
    get_publications('KMTNet', authors=authors)
    get_publications('OHP', authors=authors)
    get_publications('AAT', authors=authors)
    get_publications('CFHT', authors=authors)
    get_publications('LaSilla', authors=authors)

    get_publications('NRO', authors=authors)
    get_publications('SOFIA', authors=authors)
    get_publications('IRAM', authors=authors)
    get_publications('Noto', authors=authors)
    get_publications('TRAO', authors=authors)
    get_publications('SMA', authors=authors)
    get_publications('LOFAR', authors=authors)
    get_publications('APEX', authors=authors)

    get_publications('ALMA', authors=authors)
    get_publications('ACT', authors=authors)
    get_publications('JCMT', authors=authors)
    get_publications('VLT', authors=authors)
    get_publications('GTC', authors=authors)
    get_publications('Gemini', authors=authors)
    get_publications('TAROT', authors=authors)
    get_publications('ATCA', authors=authors)
    get_publications('MeerKAT', authors=authors)
    get_publications('VLA', authors=authors)
    get_publications('VLBA', authors=authors)
    get_publications('GBT', authors=authors)
    get_publications('EHT', authors=authors)
    get_publications('LMT', authors=authors)

    get_publications('HESS', authors=authors)

    # Return
    return


# ================================= #
# Get ADS publications from library #
# ================================= #
def get_publications_from_library():
    """
    """
    # Print header
    print('Mission    Start   2019          start-2019')
    print('                   IRAP   all    IRAP   all')

    # Query ADS for space missions
    get_library('SOHO')
    get_library('Cluster')
    get_library('DoubleStar')
    get_library('VEX')
    get_library('MEX')
    get_library('Cassini')
    get_library('Rosetta')
    get_library('GAIA')
    get_library('Hipparcos')
    get_library('AKARI')
    get_library('Herschel')
    get_library('HST')
    get_library('Planck')
    get_library('XMM')
    get_library('Chandra')
    get_library('NICER')
    get_library('Suzaku')
    get_library('INTEGRAL')

    # Query ADS for ground-based observatories
    get_library('CFHT')
    get_library('LaSilla')
    get_library('IRAM2019')
    get_library('SMA')
    get_library('LOFAR')
    get_library('ALMA')
    get_library('JCMT')
    get_library('VLT')

    # Return
    return

# =========== #
# Entry point #
# =========== #
if __name__ == '__main__':

    # Check that token has been set
    if token == '':
        print('Please set the ADS token at the top of the script before running the script.')

    # ... otherwise get the publications
    else:
        get_publications_by_query_string(authors=True)
        get_publications_from_library()
