import pandas as pd
import json


inst = pd.read_csv(
    "inputs/ads_affids.tsv",
    names=["Country", "Affid_parent", "Affid", "Inst", "Affiliation"],
    usecols=["Country", "Affid", "Inst", "Affiliation"],
    sep="\t",
    header=0,
    dtype=str,
)

inst = inst.drop_duplicates().dropna().reset_index(drop=True)

for col in inst.columns:
    inst[col] = inst[col].str.strip()

instDict = []
for index, row in inst.iterrows():
    instDict.append(
        {
            "country": row["Country"],
            "affID": row["Affid"],
            "affiliation": row["Affiliation"],
            "acronym": row["Inst"],
        }
    )

with open("outputs/astroAffiliations.json", "w") as outfile:
    json.dump(instDict, outfile, indent=2)
