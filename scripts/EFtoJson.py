import pandas as pd
import json


ef = pd.read_csv(
    "../inputs/astro_infra_EF.csv",
    names=["description_EN", "group", "start", "stop", "construction", "operations"],
    sep=";",
    header=0,
    index_col=0,
    dtype=str,
)

ef.index = ef.index.str.strip()

for col in ef.columns:
    ef[col] = ef[col].str.strip()


efdict = {}
for index, row in ef.iterrows():
    efdict[index] = {
        "description_FR": row["description_EN"],
        "description_EN": row["description_EN"],
        "unit": "percent.per.instrument",
        "group": None,
        "start": int(row["start"]),
        "stop": int(row["stop"]),
        "class": row["group"],
        "decomposition": {
            "2023": {
                "total": {
                    "total": (float(row["construction"]) + float(row["operations"])) * 1000,
                    "uncertainty": 0.8,
                },
                "construction": {
                    "total": float(row["construction"]) * 1000, # convert to kg
                    "uncertainty": 0.8,
                },
                "operations": {
                    "total": float(row["operations"]) * 1000, 
                    "uncertainty": 0.8,
                },
            }
        },
    }

with open("../outputs/facilitiesEF.json", "w") as outfile:
    json.dump(efdict, outfile, indent=2)
