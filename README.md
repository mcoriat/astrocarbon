# astroCarbon

Documentation will come soon

## Usage

```bash
# install python dependencies
pip install -r requirements.txt

# generate all targets
make all

# alternatively generate one target by giving its name
make outputs/astroAlpha_2019.csv

...
```
